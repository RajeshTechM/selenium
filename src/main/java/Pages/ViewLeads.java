package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ViewLeads extends FindLeads {

	public ViewLeads() {
	PageFactory.initElements(driver, this);	
		
	}
	
	
	public ViewLeads verifyTitleofViewLeadPage(String expectedTitle) {
		verifyTitle(expectedTitle);
       return this;

	}
	
	@FindBy(how = How.XPATH,using="//a[text()='Edit']")
	WebElement editbutton;
	public EditLead editButtonClick() {
		//WebElement editbutton = locateElement("xpath", "//a[text()='Edit']");
		click(editbutton);
		return new EditLead();
	}
	
	
	@FindBy(how = How.XPATH,using="//a[text()='Delete']")
	WebElement deletebutton;
	public MyLeads Clickdeletebutton() {
     //WebElement delete = locateElement("xpath", "//a[text()='Delete']");
		 click(deletebutton);
		return new MyLeads();
	}
	
	@FindBy(how=How.XPATH,using="//span[@id='viewLead_firstName_sp']")
	WebElement firstnametext;
	public ViewLeads getfirstnametext(String expectedText) {
		verifyExactText(firstnametext, expectedText);
		return this;
	}
	
	
}
