package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLead extends ProjectMethods {

	public EditLead() {
	  PageFactory.initElements(driver, this);
  }
	
	@FindBy(how=How.ID,using="updateLeadForm_companyName")
	WebElement companyname;
	public EditLead typeCompanyName(String companyName) {
		// WebElement companyname = locateElement("id", "updateLeadForm_companyName");
	     type(companyname, companyName);
	     return this;
	}
   
	@FindBy(how=How.XPATH,using="//input[@value='Update']")
    WebElement updatebutton;
	public ViewLead updateButton() {

	     //WebElement updateb = locateElement("xpath", "//input[@value='Update']");
	     click(updatebutton);
	     return new ViewLead();
	}
	
	
}
