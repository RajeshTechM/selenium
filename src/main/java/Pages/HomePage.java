package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage()

	{
		PageFactory.initElements(driver, this);

	}

	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA")
	WebElement crmlink;

	public MyHome clickCRMLink()

	{
		// WebElement crmlink = locateElement("linkText", "CRM/SFA");
		click(crmlink);
		return new MyHome();

	}

}
