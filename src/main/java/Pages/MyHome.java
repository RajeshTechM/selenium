package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods {

	public MyHome() {

		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement createlead;

	public CreateLead createLeadlink() {

		// WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);
		return new CreateLead();

	}

	@FindBy(how = How.LINK_TEXT, using = "Leads")
	WebElement leadslink;

	public MyLeads Leadslink() {

		// WebElement leadslink = locateElement("linktext", "Leads");
		click(leadslink);
		return new MyLeads();
	}

}
