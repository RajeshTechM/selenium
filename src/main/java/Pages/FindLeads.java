package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods {

	public FindLeads() {
		PageFactory.initElements(driver, this);
		
}
	@FindBy(how = How.XPATH,using="(//input[@name='firstName'])[3]")
	WebElement FirstName;
	public FindLeads enterFirstName (String firstName) {
	
		//WebElement FirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
	     type(FirstName, firstName);
	     return this;
}
	@FindBy(how=How.XPATH,using="//span[text()='Phone']")
	WebElement clickphonenumber;
	public FindLeads clickPhonenumberfield () {

	     //WebElement phone = locateElement("xpath","//span[text()='Phone']");
	     click(clickphonenumber);
		 return this;
		
	}
	
	@FindBy(how= How.XPATH,using="//input[@name='id']")
	WebElement LeadID;
	public FindLeads typeLeadID (String leadID) {
		
		 //WebElement LeadID = locateElement("xpath", "//input[@name='id']");
		 type(LeadID, leadID);
		 return this;
		
	}
	
	@FindBy(how=How.NAME,using="phoneNumber")
	WebElement phnumber;
	public FindLeads typephonenumber(String phonenumber) {
		//WebElement phnumber = locateElement("name", "phoneNumber");
	     type(phnumber,phonenumber);
		 return this;
		
	}	
	
	
	@FindBy(how =How.XPATH,using="//button[text()='Find Leads']")
	WebElement FindLeadsB;
	public FindLeads findLeadsButton() {
		
      //WebElement FindLeadsB = locateElement("xpath", "//button[text()='Find Leads']");
	     click(FindLeadsB);
	     try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return this;
	}
	
	@FindBy(how=How.XPATH,using="(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]")
	WebElement FirstresultingLead;
	public ViewLeads resultingLead() {
		//WebElement FirstresultingLead = locateElement("xpath", "(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]");
		///verifyExactText(FirstresultingLead, resultingExpectedText);
	     click(FirstresultingLead);
		return new ViewLeads();
	}

	@FindBy(how=How.XPATH,using="(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]")
	WebElement FirstresultingLeadtext;
	public FindLeads firstresultingLeadtext(String leadIdText) {
		 //WebElement firstresultinglead = locateElement("xpath", "(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]");
		verifyExactText(FirstresultingLeadtext, leadIdText);
		 return this;
   }
	
	@FindBy(xpath="//div[@class='x-paging-info']")
	WebElement errormsg;
	public FindLeads verifyerrormessageontable(String errormessage) {
		/*WebElement errormsg = locateElement("xpath", "//div[@class='x-paging-info']");
		 String text2 = getText(errormsg);
		*/ 
		 verifyExactText(errormsg, errormessage);
		 return this;
		
	}
	
	@FindBy(how=How.XPATH,using="(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a)[1]")
	WebElement firstleadnametext;
	public FindLeads getfirstleadnametext (String expectedText) {
		verifyExactText(firstleadnametext, expectedText);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//a[text()='Duplicate Lead']")
	WebElement duplicateleadbutton;
	public DuplicateLead clickduplicatedeletebutton() {
	click(duplicateleadbutton);	
		return new DuplicateLead();
	}
}
