package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods {

	
	public ViewLead() {
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using="viewLead_firstName_sp")
	WebElement FirstnameL;
	public ViewLead verifyFirstName(String expectedText) {
		
		//WebElement FirstnameL = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(FirstnameL,expectedText);
		return this;
	}
	
	@FindBy(id="viewLead_companyName_sp")
	WebElement companynamet;
	public ViewLead verifycompanyName(String expectedCompanyText) {
		//WebElement companynamet = locateElement("id", "viewLead_companyName_sp");
	     verifyPartialText(companynamet, expectedCompanyText);
	     return this;
	}
}
