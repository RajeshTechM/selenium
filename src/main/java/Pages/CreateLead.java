package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {

	public CreateLead() {
		PageFactory.initElements(driver, this);
	
	}
	
@FindBy(id="createLeadForm_firstName")
   WebElement typefirstname;
	public CreateLead typeFirstName(String firstname) {
		//WebElement typefirstname = locateElement("id", "createLeadForm_firstName");
		type(typefirstname, firstname);
		return this;
		
	}
	
	
	@FindBy(id="createLeadForm_lastName")
	WebElement LName;	
		
	public CreateLead typeLastName (String lastName) {
		
		WebElement LName = locateElement("id", "createLeadForm_lastName");
		type(LName, lastName);
		return this;
		
	}
	

	@FindBy(id="createLeadForm_companyName")
	WebElement CName;	
	public CreateLead typeCompanyName (String cName) {
		
		//WebElement CName = locateElement("id", "createLeadForm_companyName");
		type(CName, cName);
		return this;
		
	}

@FindBy(how = How.XPATH, using = "//input[@value='Create Lead']" )
WebElement CreateLead;
public ViewLead clickCreate() {
		

    //WebElement CreateLead= locateElement("xpath", "//input[@value='Create Lead']");
    click(CreateLead);
    return new ViewLead();
		
	}

	
}


