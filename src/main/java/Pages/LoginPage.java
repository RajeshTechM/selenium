package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);

	}

	@FindBy(id = "username")
	WebElement eleusername;

	@And("Enter the username as (.*)")
	public LoginPage enterUserName(String username) {

		// WebElement eleusername = locateElement("id", "username");
		type(eleusername, username);
		return this;

	}

	@FindBy(id = "password")
	WebElement elepassword;

	public LoginPage enterPassword(String password) {

		// WebElement elepassword = locateElement("id", "password");
		type(elepassword, password);
		return this;

	}

	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	WebElement clicklogin;

	public HomePage clickLoginButton() {

		// WebElement clicklogin = locateElement("class", "decorativeSubmit");

		click(clicklogin);

		return new HomePage();
	}

}
