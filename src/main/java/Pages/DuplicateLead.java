package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DuplicateLead extends ProjectMethods {

	public DuplicateLead() {
		
		PageFactory.initElements(driver, this);
	}
	
	public DuplicateLead getduplicateleadtitle(String expectedTitle) {
		verifyTitle(expectedTitle);
		return this;
	}
	@FindBy(how=How.XPATH,using="//input[@value='Create Lead']")
	WebElement clickcreatelead;
	public ViewLeads clickCreateLeadDuplicatePage() {
		click(clickcreatelead);
		return new ViewLeads();
		
	}
	
	
	@FindBy(how=How.ID,using="sectionHeaderTitle_leads")
	WebElement duplicatetitle;
	public void getduplicatetitle(String expectedText1) {
		verifyExactText(duplicatetitle, expectedText1);
		
	}
	
}
	
