package OWS;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ClaimCreation {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://wwwqa.warrantyauthoring.dealerconnection.com/prweb/SSOServlet");
		driver.findElementById("DEALER-WSLXloginUserIdInput").sendKeys("o-wst614");
		driver.findElementById("DEALER-WSLXloginPasswordInput").sendKeys("owstest14");
		driver.findElementByXPath("//input[@type='submit']").click();
		Thread.sleep(5000);
		driver.findElementById("DEALER-WSLXauthSuccessContinueButtonText").click();
		driver.findElementByLinkText("Enter a Claim").click();
		Thread.sleep(5000); 
		
	    WebElement iframe = driver.findElementByXPath("//iframe[@name='PegaGadget0Ifr']");
		driver.switchTo().frame(iframe);
        driver.findElementById("VIN").sendKeys("WF05XXGCC5GG25594",Keys.TAB);
        Thread.sleep(5000);
        driver.findElementById("PartyId").sendKeys("000000022",Keys.TAB);
        Thread.sleep(5000);
        driver.findElementById("RepairOrderNumber").sendKeys("PRT161");
        driver.findElementByXPath("(//a[@id='calendarIcon'])[1]").click();
        Thread.sleep(5000);
        
        
        Set<String> allwindows = driver.getWindowHandles();
		System.out.println(allwindows);
		List<String> alloptions = new ArrayList<String>();
		System.out.println(alloptions);
		alloptions.addAll(allwindows);
		
       driver.switchTo().window(alloptions.get(1));
       
       WebElement datepicker = driver.findElementByXPath("//table[@class='calendar']");
       
       List<WebElement> rows = datepicker.findElements(By.tagName("tr"));
       
       List<WebElement> columns = datepicker.findElements(By.tagName("td"));
       
       for (WebElement cell : columns) {
       	
       	if(cell.getText().equals("10") ) {
       		
       		cell.click();
       		break; 
       		}
       }        
      
       Thread.sleep(5000);
      
        driver.switchTo().window(alloptions.get(0));
      
        driver.switchTo().frame(iframe);
        driver.findElementByXPath("//input[@id='Value']").sendKeys("15987");
        
        driver.findElementByXPath("//button[contains(@name,'ClaimEntryInformation')]").click();
        //driver.findElementById("RepairLineNumber").sendKeys("05");
     
        
        WebDriverWait wait = new WebDriverWait(driver, 25);
        
        WebElement repairinformation = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@class='titleBarLabelStyleExpanded'])[1]")));
         
        driver.findElementById("RepairLineNumber").sendKeys("01");
        Thread.sleep(5000);
        WebElement claimtype = driver.findElementById("ClaimTypeDesc");
        Select Claimtypedropdown = new Select(claimtype);
        Claimtypedropdown.selectByIndex(4);
        Thread.sleep(5000);
        driver.findElementById("ProgramCode").sendKeys("G05");
        Thread.sleep(5000);
        
        //repair line completion date 
        
        
       driver.findElementByXPath("(//a[@id='calendarIcon'])[1]").click();
       Thread.sleep(5000);
       
        Set<String> allwindows1 = driver.getWindowHandles();
		//System.out.println(allwindows1);
		List<String> alloptions1 = new ArrayList<String>();
		//System.out.println(alloptions1);
		alloptions1.addAll(allwindows1);
		
		driver.switchTo().window(alloptions1.get(1));
       
       WebElement datepicker1 = driver.findElementByXPath("//table[@class='calendar']");
       
       List<WebElement> rows1 = datepicker1.findElements(By.tagName("tr"));
       
       List<WebElement> columns1 = datepicker1.findElements(By.tagName("td"));
       
       for (WebElement cell1 : columns1) {
       	
       	if(cell1.getText().equals("11") ) {
       		
       		cell1.click();
       		break; 
       		}
       }        
     
            //Thread.sleep(5000);
        	
        	driver.switchTo().window(alloptions1.get(0));
            
            driver.switchTo().frame(iframe);
          
        	driver.findElementByXPath("(//input[@id='Value'])[1]").sendKeys("15987",Keys.TAB);
        	
        	//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("CustomerConcernCode")));
        	
        	Thread.sleep(1000);
        	
        	driver.findElementById("CustomerConcernCode").sendKeys("A07");
        	
        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ConditionCode")));
        	
        	Thread.sleep(1000);
        	
        	driver.findElementById("ConditionCode").sendKeys("42");
        	
        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("CustomerComments")));
        	
        	//Thread.sleep(5000);
        
        	driver.findElementById("CustomerComments").sendKeys("Customer comments, Automation Testing");
        	
        	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("TechnicianComments")));
        	
        	//Thread.sleep(5000);
            
        	driver.findElementById("TechnicianComments").sendKeys("Technician Comments, AutomationTesting");
        	
        	//Thread.sleep(5000);
            
        	driver.findElementByXPath("//input[@type='radio']").click();
        	
        	driver.findElementById("CompletePartNumber").sendKeys("3013620");
        	
        	//Thread.sleep(5000);
        	
        	driver.findElementByXPath("(//input[@id='Value'])[2]").sendKeys("1");
        	
        	//Thread.sleep(5000);
        	
        	driver.findElementByXPath("//button[text()=' PreValidate ']").click();
        	
            /*wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()[contains(.,'DEC0007')]]")));
        	
            String runtime = driver.findElementByXPath("//label[text()[contains(.,'DEC0007')]]").getText(); 
        	
        	////label[text()[contains(.,'DEC0007')]] - It should be a text. 
        	
        	if(runtime.contains("DEC0007") ) {
        		System.out.println("Claim Pre-validated successfully");
        	*/	
        		
        		//List<WebElement> alertmessages = driver.findElementsByXPath("//td[@class='custom_CaseHeaderAlerts']/div/div/div/div/table");
        		
        		List<WebElement> alertmessages = driver.findElementsByXPath("//td[@class='custom_CaseHeaderAlerts']/div/div/div/div/table//tr/td/label");
        	
        		System.out.println("Number of alerts are :"+alertmessages.size());
        		
        		List<String> alertmessagestext = new ArrayList<String>();
        		
        		for (String alerts : alertmessagestext) {
					alertmessagestext.add(alerts);
				System.out.println(alertmessagestext);
					
        			
				}
        		
        		
        		//td[@class='custom_CaseHeaderAlerts']/div/div/div/div/table//tr/td/label
        		
        		/*WebDriverWait wait1 = new WebDriverWait(driver, 25);
                
                WebElement submitbutton = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()[contains(.,'DEC0007')]]")));
                
                driver.findElementByXPath("//button[text()=' Submit ']\"").click();
                
              WebDriverWait wait3 = new WebDriverWait(driver, 15);
                
              WebElement textaftersubmit = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("label[text()='The repair line has been submitted']")));

              if (textaftersubmit.getText().equals("The repair line has been submitted")) {
            	  System.out.println("The Claims has been successfully submitted without any errors");
            	  
            	 }
              
              else {
            	  
            	System.out.println("The Claim submitted has errors and can not be processed");
            	  
              }
             
                
                }
        		
        	else 
        			
        		{
        			System.out.println("Claim is not Pre-validated Successfully, It has some errors");
        			
        		}
        	
*/        	
        	}
        	
        }        
	

