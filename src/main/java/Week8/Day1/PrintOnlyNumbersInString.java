package Week8.Day1;



public class PrintOnlyNumbersInString {

	public static void main(String[] args) {
		
		String str= "hello 1 World 2 Welcome 3";
		
	//	int length = str.length();
		
		//char[] chr = str.toCharArray();
		
		for (int i = 0; i < str.length(); i++) {
			
			char ch = str.charAt(i);
			
			//character is the wrapper class. 
			
		  if(Character.isDigit(ch)) {
			  
			  System.out.println(ch);
			  
		  }
			
			
		}
		
	}
	
	
}
