package Week8.Day1;

import java.util.ArrayList;
import java.util.Collections;

public class RearrangeNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//String str = "19673";
		
		 ArrayList<String> arraylist = new ArrayList<String>();
		   arraylist.add("19673");

		   /*Unsorted List: ArrayList content before sorting*/
		   System.out.println("Before Sorting:");
		   for(String str: arraylist){
				System.out.println(str);
			}

		   /* Sorting in decreasing order*/
		   Collections.sort(arraylist, Collections.reverseOrder());
		   
		   System.out.println("ArrayList in descending order:");
		   for(String str: arraylist){
				System.out.println(str);
		
		
	}
	}
	
	}
