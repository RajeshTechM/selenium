package PageTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods {

	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateTestLead"; 
		testCaseDesc = "Create a New Lead using page object Model"; 
		category = "Smoke"; 
		author = "Rajesh";
		//node ="Leads";
		excelFileName = "CL" ;
		
	}	
	
	@Test (dataProvider ="fetchData")
	public void createLead (String username,String password,String cName,String firstname,String lastName) {
	new LoginPage()
	.enterUserName(username).enterPassword(password).clickLoginButton()
	.clickCRMLink()
	.createLeadlink().typeFirstName(firstname).typeLastName(lastName).typeCompanyName(cName)
	.clickCreate().verifyFirstName(firstname);

	}
	
}
