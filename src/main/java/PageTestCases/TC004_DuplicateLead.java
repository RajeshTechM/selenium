package PageTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC004_DuplicateTestLead"; 
		testCaseDesc = "verifying duplicate lead using page object Model"; 
		category = "Smoke"; 
		author = "Rajesh";
		//node ="Leads";
		excelFileName = "DuplicatLead1";
		
	}	
	
	@Test (dataProvider ="fetchData")
	public void createLead (String username,String password,String expectedText,String expectedTitle,String expectedText1) {
	new LoginPage()
	.enterUserName(username).enterPassword(password).clickLoginButton()
	.clickCRMLink()
	.Leadslink()
	.findLeadslink()
	.getfirstleadnametext(expectedText).resultingLead()
	.clickduplicatedeletebutton().getduplicateleadtitle(expectedTitle)
	.clickCreateLeadDuplicatePage().getfirstnametext(expectedText1);
	
}

}


