package PageTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_EditLead"; 
		testCaseDesc = "Editing company name of the Lead using page object Model"; 
		category = "Smoke"; 
		author = "Rajesh";
		//node ="Leads";
		excelFileName = "EL" ;
		
	}	
	
	@Test (dataProvider ="fetchData")
	public void editingCompanyNameofLead (String username,String password,String firstName,String expectedTitle,String companyName,String expectedCompanyText) {
	new LoginPage()
	.enterUserName(username).enterPassword(password).clickLoginButton()
	.clickCRMLink()
	.Leadslink()
	.findLeadslink().enterFirstName(firstName)
	.findLeadsButton()
	.resultingLead()
	.verifyTitleofViewLeadPage(expectedTitle)
	.editButtonClick()
	.typeCompanyName(companyName)
	.updateButton()
	.verifycompanyName(expectedCompanyText);

	}

	
	
}
