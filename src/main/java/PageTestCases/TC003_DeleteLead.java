package PageTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_DeleteTestLead"; 
		testCaseDesc = "Deleting Lead using page object Model"; 
		category = "Smoke"; 
		author = "Rajesh";
		//node ="Leads";
		excelFileName = "DL" ;
		
	}	
	
	@Test (dataProvider ="fetchData")
	public void createLead (String username,String password,String phonenumber,String leadIdText,String leadID,String errormessage) {
	new LoginPage()
	.enterUserName(username).enterPassword(password).clickLoginButton()
	.clickCRMLink()
	.Leadslink()
	.findLeadslink().clickPhonenumberfield().typephonenumber(phonenumber).findLeadsButton()
	.firstresultingLeadtext(leadIdText).resultingLead()
	.Clickdeletebutton()
	.findLeadslink()
	.typeLeadID(leadID)
	.findLeadsButton().verifyerrormessageontable(errormessage);
	}
	
}


