package testcase1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public final class TC007_ZoomCar extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC007_ZoomCar"; 
		testCaseDesc = "Test2 ZoomCar"; 
		category = "Smoke"; 
		author = "Rajesh";

	}
    @Test
	public void ZoomCar ()  {

        startApp("chrome", "https://www.zoomcar.com/chennai");
        WebElement journeylink = locateElement("linkText", "Start your wonderful journey");
        click(journeylink);
        WebElement Pickuppoint = locateElement("xpath", "//div[@class='items'][4]");
        click(Pickuppoint);
        WebElement NextB = locateElement("xpath", "//button[text()='Next']");
        click(NextB);
     // Get the current date
     		Date date = new Date();
     		//System.out.println(date);
     // Get only the date (and not month, year, time etc)
     		DateFormat sdf = new SimpleDateFormat("dd");
     		//System.out.println(sdf);
     // Get today's date
     		String today = sdf.format(date);
     		//System.out.println(today);
     // Convert to integer and add 1 to it
     		int tomorrow = Integer.parseInt(today)+1;
     // Print tomorrow's date
     		System.out.println(tomorrow);

    List<WebElement> alldays = driver.findElementsByXPath("//div[@class='day']");
    
    for (WebElement Days : alldays) {
		
    	alldays.add(Days);
    	
	}
    
     int size = alldays.size();
     System.out.println(size);
     
    
	}
      
    	
	}

	
