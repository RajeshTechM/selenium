package testcase1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002_CreateTestLead extends ProjectMethods {

	@BeforeTest(groups = { "smoke" })
	public void setData() {
		testCaseName = "TC002_CreateTestLead";
		testCaseDesc = "Create a New Lead";
		category = "Smoke";
		author = "Rajesh";
		excelFileName = "CL";
	}

	@Test(groups = { "smoke" }, dataProvider = "fetchdata")
	public void CreateLead(String uname, String password, String companyName, String fname, String lname)
			throws InterruptedException {

		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, password);

		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);

		WebElement crmlink = locateElement("linkText", "CRM/SFA");
		click(crmlink);

		WebElement FindLeads = locateElement("linkText", "Create Lead");
		click(FindLeads);

		WebElement CName = locateElement("id", "createLeadForm_companyName");
		type(CName, companyName);

		WebElement FName = locateElement("id", "createLeadForm_firstName");
		type(FName, fname);

		WebElement LName = locateElement("id", "createLeadForm_lastName");
		type(LName, lname);

		WebElement Source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(Source, "Conference");

		WebElement MCompaign = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(MCompaign, "Automobile");

		WebElement Industry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingIndex(Industry, 3);

		WebElement CreateLead = locateElement("xpath", "//input[@value='Create Lead']");
		click(CreateLead);

		WebElement FirstnameL = locateElement("id", "viewLead_firstName_sp");
		// getText(FirstnameL);

		// verify exact match

		verifyExactText(FirstnameL, fname);

	}

	/*
	 * @DataProvider (name ="positive") public Object[][] fetchdata() throws
	 * IOException { //To read the changing file. we can not declare an array size
	 * as constant return ReadExcel.FetchDataExcel(excelFileName);
	 * 
	 * Object [][] data = new Object[2] [3];
	 * 
	 * data[0][0] = "Wipro"; data[0][1] = "Bharthi"; data[0][2] = "Kannan";
	 * 
	 * data[1][0] = "Tech Mahindra"; data[1][1] = "Rajesh"; data[1][2] = "Kumar";
	 * return data; }
	 */
	/*
	 * @DataProvider (name ="negative") public void fectchNegData() {
	 * 
	 * }
	 * 
	 */ }
