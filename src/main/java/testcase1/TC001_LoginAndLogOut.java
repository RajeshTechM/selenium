package testcase1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC001_LoginAndLogOut extends ProjectMethods{
	
	@BeforeTest (groups = {"smoke" } ) 
	public void setData() {
		testCaseName = "TC001_LoginAndLogOut"; 
		testCaseDesc = "Login and LogOut"; 
		category = "Smoke"; 
		author = "Rajesh";
		excelFileName = "CL";
		
		
	}	
	
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
		//closeBrowser();
		
		
	}
	
}







