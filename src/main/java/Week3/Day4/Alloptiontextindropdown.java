package Week3.Day4;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Alloptiontextindropdown {

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//to maximize the browser
		driver.manage().window().maximize();
		//to get URL
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("Demosalesmanager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@type='submit']").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']").click();
		driver.findElementByXPath("//span[text()='Advanced']").click();
		
		//No Such element thrown
		WebElement country = driver.findElementByXPath("//input[@class=' x-form-text x-form-field x-form-focus']");
		Select dropdowncl = new Select(country);
		List<WebElement> lst = dropdowncl.getOptions();
        System.out.println(lst);

	}

}
