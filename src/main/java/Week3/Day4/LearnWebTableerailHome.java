package Week3.Day4;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnWebTableerailHome {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// maximize the window
		driver.manage().window().maximize();
		//URL Invoking
		driver.get("https://erail.in/");
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("TBM",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementByXPath("//input[@id='txtStationTo']").sendKeys("VPT",Keys.TAB);
		driver.findElementById("chkSelectDateOnly").click();
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> tablerow = table.findElements(By.tagName("tr"));
		System.out.println(tablerow.size());
		
		WebElement firstrow = tablerow.get(0);
		List<WebElement> tablecolumn = firstrow.findElements(By.tagName("td"));
		System.err.println(tablecolumn.size());
		String trainname = tablecolumn.get(1).getText();
		System.out.println(trainname);
		WebDriverWait wait= new WebDriverWait(driver, 10);
		
		Boolean acutal = wait.until(ExpectedConditions.textToBePresentInElement(tablecolumn.get(0),"16127"));
		
	}

}
