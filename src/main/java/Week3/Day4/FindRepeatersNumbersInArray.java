package Week3.Day4;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class FindRepeatersNumbersInArray {

	public static void main(String[] args) {
		
		int arr[]= {13,65,15,67,88,65,13,99,67,13,65,87,13,5};
		Set<Integer> lset=new LinkedHashSet<Integer>();
		int size=arr.length;
		
		for (int i=0;i<size;i++)
		{
			int count=0;
			for(int j=1;j<size;j++)
			{
				if(arr[i]==arr[j])
				{
					count=count+1;
				}
			}
			if(count>1)
			{
				lset.add(arr[i]);
				
			}
		}
		List<Integer> lst=new ArrayList<Integer>();
		lst.addAll(lset);
		System.out.println("Repeated numbers are "+lst);
}

}

