package Week3.Day4;

import org.openqa.selenium.chrome.ChromeDriver;

public class DeselectChecked {

	public static void main(String[] args) {
		
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	
	driver.manage().window().maximize();
	driver.get("http://testleaf.herokuapp.com/pages/checkbox.html");
	
	driver.findElementByXPath("//div//label[text()='Select the languages that you know?']/following::input[1]").click();
	driver.findElementByXPath("//div/label[text()='Confirm Selenium is checked']/following::input[1]").click();
	
	driver.findElementByXPath("//div//label[text()='Select the languages that you know?']/following::input[1]").click();
	driver.findElementByXPath("//div/label[text()='DeSelect only checked']/following::input[2]").click();
	driver.findElementByXPath("//div/label[text()='Select all below checkboxes ']/following::input[1]").click();
	driver.findElementByXPath("//div/label[text()='Select all below checkboxes ']/following::input[2]").click();
	driver.findElementByXPath("//div/label[text()='Select all below checkboxes ']/following::input[3]").click();
	driver.findElementByXPath("//div/label[text()='Select all below checkboxes ']/following::input[4]").click();
	driver.findElementByXPath("//div/label[text()='Select all below checkboxes ']/following::input[5]").click();
	driver.findElementByXPath("//div/label[text()='Select all below checkboxes ']/following::input[6]").click();
	
	}

}
