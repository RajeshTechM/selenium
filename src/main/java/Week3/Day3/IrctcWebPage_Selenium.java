package Week3.Day3;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcWebPage_Selenium {

	

	public static void main(String[] args) throws InterruptedException {
	
	 System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	 ChromeDriver driver = new ChromeDriver();
	 //to maximize the window
	 driver.manage().window().maximize();
	 //to get URL
	 driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");

	 driver.findElementById("userRegistrationForm:userName").sendKeys("rajeshliving");
	 driver.findElementById("userRegistrationForm:password").sendKeys("$Electronics123");
	 driver.findElementById("userRegistrationForm:confpasword").sendKeys("$Electronics123");
	 
	 //Creating an object for drop down since there are two operations involved. 
	 WebElement SQoption = driver.findElementById("userRegistrationForm:securityQ");
	 String text = SQoption.getText();
	 System.out.println(text);
	 
	 Select SQdropdown = new Select(SQoption);
	 List<WebElement> lsd = SQdropdown.getOptions();
	 int size = lsd.size();
	 SQdropdown.selectByIndex(3);
	 
	 driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Sachin");
      //Creating an object for drop down since there are two operations involved.
	WebElement PLanguageoption = driver.findElementById("userRegistrationForm:prelan");
	Select Languagedropdown =  new Select(PLanguageoption);
	List<WebElement> lsd1 = Languagedropdown.getOptions();
	int size2 = lsd1.size(); 
	//Languagedropdown.selectByIndex(1);
			
	 driver.findElementById("userRegistrationForm:firstName").sendKeys("Rajesh");
	 driver.findElementById("userRegistrationForm:gender:0").click();
	 driver.findElementById("userRegistrationForm:maritalStatus:0").click();
	 
	WebElement Day =  driver.findElementById("userRegistrationForm:dobDay");
	Select dropdownday = new Select(Day);
	//List<WebElement> lsd2 = dropdownday.getOptions(); 
	//int size3 = lsd2.size(); 
	dropdownday.selectByVisibleText("18");
    WebElement Month = driver.findElementById("userRegistrationForm:dobMonth");
	Select dropdownm = new Select(Month);
	List<WebElement> lsd3 = dropdownm.getOptions(); 
	int size4 = lsd3.size(); 
	dropdownm.selectByVisibleText("MAY");
    WebElement Year = driver.findElementById("userRegistrationForm:dateOfBirth");
	Select dropdowny = new Select(Year);
	List<WebElement> lsd4 = dropdownm.getOptions(); 
	int size5 = lsd4.size(); 
	dropdowny.selectByVisibleText("1988");
	WebElement Occ = driver.findElementById("userRegistrationForm:occupation");
	Select Occdropdown = new Select(Occ); 
	Occdropdown.selectByVisibleText("Private");
	WebElement Country = driver.findElementById("userRegistrationForm:countries");
	Select Countryoption = new Select(Country);
	Countryoption.selectByVisibleText("India");
    driver.findElementById("userRegistrationForm:email").sendKeys("rajeshliving@gmail.com");
	driver.findElementById("userRegistrationForm:mobile").sendKeys("9500543516");
	WebElement Nationality = driver.findElementById("userRegistrationForm:nationalityId");
	Select nationdropdown = new Select(Nationality);
	nationdropdown.selectByVisibleText("India");
	driver.findElementById("userRegistrationForm:address").sendKeys("20,F3");
	driver.findElementById("userRegistrationForm:pincode").sendKeys("600063",Keys.TAB);
	Thread.sleep(5000);
	WebElement city= driver.findElementById("userRegistrationForm:cityName");
	Select cityoption = new Select(city);
	cityoption.selectByIndex(1);
	Thread.sleep(5000);
	WebElement postoffice = driver.findElementById("userRegistrationForm:postofficeName");
	Select postofficeoption = new Select(postoffice);
	postofficeoption.selectByIndex(2);
	driver.findElementById("userRegistrationForm:landline").sendKeys("04566261129");
	driver.findElementById("userRegistrationForm:resAndOff:1").click();
	driver.findElementById("userRegistrationForm:resAndOff:1").sendKeys("F4,140");
	WebElement ResCountry = driver.findElementById("userRegistrationForm:countrieso"); 
    Select Rescountryoption = new Select(ResCountry);
    Rescountryoption.selectByVisibleText("India");
    driver.findElementById("userRegistrationForm:pincodeo").sendKeys("628905",Keys.TAB);
    Thread.sleep(5000);
    WebElement OffCity = driver.findElementById("userRegistrationForm:cityNameo");
    Select OffCityop = new Select(OffCity);
    OffCityop.selectByVisibleText("Tuticorin");
    Thread.sleep(5000);
    WebElement Postofficeres = driver.findElementById("userRegistrationForm:postofficeNameo");
    Select officeres = new Select(Postofficeres);
    List<WebElement> lsd10 = officeres.getOptions();
    int size3= lsd10.size();
    //for (WebElement eachelement : lsd10) {
    
    	System.out.println(size3);
		
	//}
    
   officeres.selectByIndex(size3-3); 
   driver.findElementById("userRegistrationForm:landlineo").sendKeys("0456626129");
    

	}

}
