package Week3.Day3;



import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class FirstSeleniumProgram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//We have to set the bindings. 
		//meditator to talk with chrome browser since its a third party browser. 
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	     ChromeDriver driver = new ChromeDriver();
	     // maximize
	    	 
	     driver.manage().window().maximize();
	      
	     driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	     
	     // get URL
	     driver.get("http://leaftaps.com/opentaps");
	    // to find the elements inside the webpage
	     //to enter username type - > textbox - > sendkeys
	     
	     try {
			driver.findElementById("username1").sendKeys("Demosalesmanager");
		} catch (NoSuchElementException e) {
			System.out.println("No Such element thrown");
			//throw new RuntimeException();
		}
		driver.findElementById("password").sendKeys("crmsfa");
	     // to click on login button
	     driver.findElementByClassName("decorativeSubmit").click();
	     driver.findElementByLinkText("CRM/SFA").click();
	     driver.findElementByLinkText("Create Lead").click();
	     driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
	     driver.findElementById("createLeadForm_firstName").sendKeys("Rajesh");
	     driver.findElementById("createLeadForm_lastName").sendKeys("Kumar");
	     
	     //Creating an object for drop down since there are two operations involved. 
	    WebElement src= driver.findElementById("createLeadForm_dataSourceId");
	    
	    //select providing helper methods to select and deselect option
	    Select dropdown = new Select (src); 
	    
	    //
	     
	    dropdown.selectByVisibleText("Employee"); 
	    
	   WebElement src1= driver.findElementById("createLeadForm_marketingCampaignId"); 
	    
	    Select dropdown1 = new Select (src1);
	  
	 List<WebElement> lsd = dropdown1.getOptions();
	 
	    int size = lsd.size();
	    dropdown1.selectByIndex(size-2); 
	     //driver.findElementByName("submitButton").click();
	   
	     
	}

}
