package wdMethodsOld;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver  = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver  = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" launched successfully");
		} catch (NoSuchElementException e) {
			System.err.println("NoSuch element exception has thrown");
			throw new RuntimeException(); 
		}  catch (StaleElementReferenceException e) {
			System.err.println("Stale Element exception has thrown");
		} 	
		finally {
		 takeSnap();
	}
		}



	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "class": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);	
		}
		return null;
	}

	
	public WebElement locateElement(String locValue) {		
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered Successfully");
		takeSnap();
	}


	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}

	
	public String getText(WebElement ele) {
	   String text = ele.getText();
	   System.out.println("The Text is:"+text);
	   takeSnap();
	   return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select sel1 = new Select(ele);
		sel1.selectByVisibleText(value);
		System.out.println("The selected value is:"+value);
	    takeSnap();
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select sel2 = new Select(ele);
		sel2.selectByIndex(index);
		System.out.println("The Selected Index is:"+index);
        takeSnap();
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
	     driver.switchTo().window(expectedTitle).getTitle();
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		
		String ExactText = ele.getText();
		if(ExactText.equals(expectedText))
		{
			System.out.println("The text is :" +expectedText + "exactly matched");
			} else 
				
			{
			System.out.println("The text is: " +expectedText+ " not exactly matched");
			}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String PartialText = ele.getText();
		if(PartialText.contains(expectedText)) {
			System.out.println("The Partial text: "+ expectedText+" is matched");
			}
		else {
			System.out.println("The Partial text:"+expectedText+" is not matched");
			
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
	  if(ele.isSelected()) {
		  System.out.println("The Element "+ ele + "is selected");
		  
	  }
		  else {
			  System.out.println("The Element" + ele + "is not selected");
		  }

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isDisplayed()) {
			  System.out.println("The Element "+ ele + "is Displayed");
			  
		  }
			  else {
				  System.out.println("The Element "+ ele + "is not Displayed");
			  }
		
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> ListofWindows = new ArrayList<String>();
		ListofWindows.addAll(windowHandles);
		driver.switchTo().window(ListofWindows.get(index));

	}

	@Override
	public void switchToFrame(WebElement ele) {
	 driver.switchTo().frame(ele);

	}

	@Override
	public void acceptAlert() {
	 driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
	driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		String AlertText = driver.switchTo().alert().getText();
		return AlertText;
	}


	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img"+i+".png");		
		FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
	  driver.quit();
     }

}
