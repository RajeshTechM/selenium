package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
public class TC002_CreateTestLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateTestLead"; 
		testCaseDesc = "Create a New Lead"; 
		category = "Smoke"; 
		author = "Rajesh";
		
	}

    @Test (invocationCount=2, priority =1)
	public void CreateLead () throws InterruptedException {
     WebElement FindLeads = locateElement("linkText", "Create Lead");
     click(FindLeads);
     
     WebElement CName = locateElement("id", "createLeadForm_companyName");
     type(CName, "Tech Mahindra");
     
     WebElement FName = locateElement("id", "createLeadForm_firstName");
     type(FName, "Rajesh");
     
     WebElement LName = locateElement("id", "createLeadForm_lastName");
     type(LName, "Alagar");
     
     WebElement Source= locateElement("id", "createLeadForm_dataSourceId");
     selectDropDownUsingText(Source, "Conference");
     
     WebElement MCompaign = locateElement("id", "createLeadForm_marketingCampaignId");
     selectDropDownUsingText(MCompaign, "Automobile");
     
     WebElement Industry = locateElement("id", "createLeadForm_industryEnumId");
     selectDropDownUsingIndex(Industry, 3);
     
     WebElement CreateLead= locateElement("xpath", "//input[@value='Create Lead']");
     click(CreateLead);
     
     WebElement FirstnameL = locateElement("id", "viewLead_firstName_sp");
     //getText(FirstnameL);
     
    //verify exact match 
    
     verifyExactText(FirstnameL, "Rajesh");
    
     
    }
    	 
    	 
     }
