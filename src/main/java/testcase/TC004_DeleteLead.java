package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods {
	
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_DeleteLead"; 
		testCaseDesc = "Editing first resulting Lead"; 
		category = "Smoke"; 
		author = "Rajesh";

	}
	
    @Test (dependsOnMethods= {"testcase.TC002_CreateTestLead.CreateLead"})
	public void DeleteLead () throws InterruptedException {
 
    	 WebElement Leads = locateElement("linkText", "Leads");
	     click(Leads);
	    
	     WebElement FindLeadsL = locateElement("linkText", "Find Leads");
	     click(FindLeadsL);	
	     
	     WebElement phone = locateElement("xpath","//span[text()='Phone']");
	     click(phone);
	     
	     WebElement phnumber = locateElement("name", "phoneNumber");
	     type(phnumber,"9500543516");
	    
	     WebElement findleadsbutton = locateElement("xpath", "//button[text()='Find Leads']");
		 click(findleadsbutton);
		 Thread.sleep(5000);
		
		 WebElement firstresultinglead = locateElement("xpath", "(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]");
		 String text = getText(firstresultinglead);
		 click(firstresultinglead);
		 
		 WebElement delete = locateElement("xpath", "//a[text()='Delete']");
		 click(delete);
		 
		 WebElement findleads = locateElement("linkText", "Find Leads");
		 click(findleads);
		 
		 WebElement LeadID = locateElement("xpath", "//input[@name='id']");
		 type(LeadID, text);
		 
		 WebElement findleadsbutton1 = locateElement("xpath", "//button[text()='Find Leads']");
		 click(findleadsbutton1);
		 
		 Thread.sleep(5000);
		 
		 WebElement errormsg = locateElement("xpath", "//div[@class='x-paging-info']");
		 String text2 = getText(errormsg);
		 
		 verifyExactText(errormsg, "No records to display");
		 
	}
	
}
