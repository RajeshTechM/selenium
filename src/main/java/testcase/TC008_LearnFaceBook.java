package testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC008_LearnFaceBook extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC008_Facebook"; 
		testCaseDesc = "Test1 Facebook Like"; 
		category = "Smoke"; 
		author = "Rajesh";

	}
    @Test
	public void Facebooklike () throws InterruptedException {
    	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
    	ChromeOptions options = new ChromeOptions();
    	options.addArguments("--disable-notifications");
    	ChromeDriver driver = new ChromeDriver(options);
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	driver.get("https://www.facebook.com/");
    	driver.manage().window().maximize();
    	Thread.sleep(5000);
		driver.findElementById("email").sendKeys("rajeshliving@gmail.com");
		driver.findElementById("pass").sendKeys("$technetronics12");
		driver.findElementByXPath("//input[@value='Log In']").click();
		Thread.sleep(5000);
		driver.findElementByXPath("//input[@class='_1frb']").sendKeys("TestLeaf");
		driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		
		
        String text = driver.findElementByXPath("//div[@class='_6rbb']//a/div").getText();
        System.out.println(text);
          
          
        String LikeButtontext = driver.findElementByXPath("(//div[@class='_3ko9']/span/button)[1]").getText(); 
        System.out.println(LikeButtontext);
         
        if (LikeButtontext.equalsIgnoreCase("Like")) {
          
        	driver.findElementByXPath("(//div[@class='_3ko9']/span/button)[1]").click();
        	System.out.println(driver.findElementByXPath("(//div[@class='_3ko9']/span/button)[1]").getText());
        	Thread.sleep(5000);
        	
        } else 
        
        {
        	
        	System.out.println("Its already liked and the text of button is :"+LikeButtontext);
        	
        }
		
		driver.findElementByXPath("(//div[@class='_2xjf']/a/div)[1]").click();
		String Title = driver.getTitle();
		if (Title.contains("TestLeaf")) {
			System.out.println("The Title of the Page contains TestLeaf");
			
		} else {
			System.out.println("The Title of the Page does not contain TestLeaf");
		}
		String Likesforthepage = driver.findElementByXPath("//div[text()[contains(.,'people like this')]]").getText();
		System.out.println(Likesforthepage);
        
	}
	
}
