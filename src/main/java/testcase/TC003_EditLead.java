package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_EditLead"; 
		testCaseDesc = "Editing Lead"; 
		category = "Smoke"; 
		author = "Rajesh";
		
	}
		//Depends on another Method which is in another class. so we have to use package name, class name and method name. 
		@Test (enabled = false, priority = 2 )
		public void EditLead () throws InterruptedException {
	     WebElement Leads = locateElement("linkText", "Leads");
	     click(Leads);
	    
	     WebElement FindLeadsL = locateElement("linkText", "Find Leads");
	     click(FindLeadsL);
	     
	     WebElement FirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
	     type(FirstName, "Rajesh");
	     
	     WebElement FindLeadsB = locateElement("xpath", "//button[text()='Find Leads']");
	     click(FindLeadsB);
	     
	     Thread.sleep(5000);
	     
	     WebElement FirstresultingLead = locateElement("xpath", "(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]");
	     click(FirstresultingLead);
	     
	     verifyTitle("View Lead | opentaps CRM"); 
	     
	     WebElement Edit = locateElement("xpath", "//a[text()='Edit']");
	     click(Edit);
	     
	     WebElement companyname = locateElement("id", "updateLeadForm_companyName");
	     type(companyname, "Infosys");
	     
	     WebElement updateb = locateElement("xpath", "//input[@value='Update']");
	     click(updateb);
	     
	     WebElement companynamet = locateElement("id", "viewLead_companyName_sp");
	     
	    verifyPartialText(companynamet, "Inf");
	    
	     
	    }
	    	 
	    	 
	     }

	

