package Week2.Day1;

public class MyPhone {
	
	public static void main(String[] args) {
		
		
		//Creating an Object Class Name Reference Name = New ClassName(); 
		
		IPhone ph=new IPhone();   //creating an IPhone object to call an another class. 
		/*ph.AddContact();    // calling the different methods. AddContact,SiriVoice, SendSMS, Camera.  --InHeritance. 
		ph.SiriVoice();
		ph.SendSMS();
		ph.camera();*/
		 // overriding the method from different classes, Overloading same class. 
		
		MobilePhone m1=new MobilePhone();
		m1.AddContact();
		MobilePhone m2=new IPhone();
		m2.AddContact();
		
	}

}
