package Week2.Day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnArrayList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	List<String> myPhone = new ArrayList<String>(); 
	myPhone.add("Nokia"); 
	myPhone.add("HTC");
	myPhone.add("Nokia");
	myPhone.add("Samsung");
	myPhone.add("IPhone"); 
	int size = myPhone.size(); 
	System.out.println("The count of mobiles are "+size);
	// Get (size) To get the list of phones -2  is used to get before the last phone. 
	System.out.println("Last before : "+myPhone.get(size-2));
	Collections.sort(myPhone);
	System.out.println("Sorted list");
	for (String eachmobile : myPhone) 
	{
	
		System.out.println(eachmobile);
	}
	
	
	}

}
