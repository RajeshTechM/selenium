package Week2.Day1;

public class MobilePhone implements MobileDesign, TelecomDesign  // CLASS AND INTERFACE COMMUNICATES WITH IMPLEMENTS KEYWORD 

// YOU CAN HAVE MULTIPLE INTEFACE IMPLEMENTED IN A CLASS. 

{

	public void SendSMS()

	{

		System.out.println("Sending SMS");
	}

	public void AddContact()

	{
		System.out.println("Adding a Contact-MobilePhone");

	}

	public void camera()

	{
		System.out.println("Taking Photos");

	}

	@Override  
	public void support4G() 
	
	{
		
		System.out.println(" 4G Supported");
		
	}

	@Override
	public void enableWifi() {
		System.out.println("Wifi Enabled");
	}

	@Override
	public void enableSIM() {
		System.out.println("SIM enabled");
		
	}

}
