package Week2.Day1;

public class CallInterface {

	public static void main(String[] args) 
	
	{
		
		//MobileDesign md = new MobileDesign(); we can not create an object for Interface.
		MobilePhone mp = new MobilePhone(); 
		mp.support4G();
		mp.enableWifi();
		mp.enableSIM();
	
		TelecomDesign TD = new MobilePhone(); 
		TD.enableSIM();
		
	}

}
