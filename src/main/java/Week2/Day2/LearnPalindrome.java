package Week2.Day2;

import java.util.Scanner;

public class LearnPalindrome {

	public static void main(String[] args) {
		
		
		 String original, reverse = ""; // Objects of String class  
	      Scanner in = new Scanner(System.in);   
	      System.out.println("Enter a string/number to check if it is a palindrome");  
	      original = in.nextLine();   
	      int length = original.length();   
	      for ( int i = length - 1; i >= 0; i-- )  {
	    	  
	         reverse = reverse + original.charAt(i);
	      }
	      // The method charAt(int index) returns the character at the specified index. 
	      //The index value should lie between 0 and length()-1. For e.g. s.charAt(0) would return the first character of the string �s�.
	      if (original.equals(reverse)) {   //comparing with reversed string 
	         System.out.println("Entered string/number is a palindrome.");
	      } 
	      else { 
	         System.out.println("Entered string/number isn't a palindrome.");   	
	}

	}
	}
	
