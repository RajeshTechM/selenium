package Week2.Day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class LearnSetClassWork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
 Set<String> mobiles = new LinkedHashSet<String>();
  mobiles.add("Samsung"); 
  mobiles.add("Nokia");
  mobiles.add("IPhone");
  mobiles.add("Moto");
  mobiles.add("LG"); 
  mobiles.add("Samsung");  // have to try this program with HashSet and TreeSet. 
  
  int size = mobiles.size(); 
  System.out.println("The Count of the Mobiles :"+size);
  
 for (String eachchar : mobiles) {

	 System.out.println(eachchar);
	 
} 

 List<String> myList = new ArrayList <String> ();
 myList.addAll(mobiles);
 String str = myList.get(0);  // To get the first purchased mobiles. 
 System.out.println("The First Purchased Mobile is :"+str);
 
	}

	
	
}
