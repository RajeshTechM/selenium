package Week5.Day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReports {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
    // To generate HTML reports we have to create an object.  by default it generates empty HTML file
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html"); 
		html.setAppendExisting(true);
	// to edit HTML reports below line is needed. 
		ExtentReports extent = new ExtentReports(); 
		extent.attachReporter(html);
		//Test Case Level
		
	  ExtentTest test = extent.createTest("TC002_CreateTestLead", "Create a New TestLead");
	  // Test Design steps level
	  test.assignCategory("Smoke");
	  test.assignAuthor("Rajesh"); 
      test.pass("The Chrome Browser Launched Succssfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
      test.pass("The Username entered Successfully");
      test.fail("The Chrome Browser Launched Succssfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
      test.pass("The Submit button clicked successfully");
// without flush it will not generate reports.       
      extent.flush();
      
	}

}
