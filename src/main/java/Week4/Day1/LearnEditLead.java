package Week4.Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnEditLead {

	public static void main(String[] args) throws InterruptedException {
	
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver(); 
		// Implicit wait
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("Demosalesmanager");
	    driver.findElementById("password").sendKeys("crmsfa");
	    driver.findElementByXPath("//input[@type='submit']").click();
	    driver.findElementByLinkText("CRM/SFA").click();
	    driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		Thread.sleep(5000);
	    driver.findElementByXPath("(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]").click();
	    String PageTitle = driver.findElementByXPath("//div[@class='x-panel-header sectionHeaderTitle']").getText();
	    System.out.println("The Title of the Page is:"+PageTitle);
	    
		driver.findElementByXPath("//div[@class='frameSectionExtra']/a[3]").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Tech Mahindra");
		driver.findElementByXPath("//input[@value='Update']").click();
		
		String CName = driver.findElementByXPath("//span[@id='viewLead_companyName_sp']").getText();
		String expected = "Tech";
		
		if(CName.contains(expected)) {
			
			System.out.println("The Partial text is matched");
		}
			else {
				
				System.out.println("The Partial text is not matched");
			}
		}
	}

