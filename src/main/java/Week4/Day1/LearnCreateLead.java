package Week4.Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnCreateLead {

	public static void main(String[] args) throws InterruptedException {
		
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	driver.get("http://leaftaps.com/opentaps/control/main");
	driver.findElementById("username").sendKeys("Demosalesmanager");
    driver.findElementById("password").sendKeys("crmsfa");
    driver.findElementByXPath("//input[@type='submit']").click();
    driver.findElementByLinkText("CRM/SFA").click();
    driver.findElementByLinkText("Create Lead").click();
    
    driver.findElementById("createLeadForm_companyName").sendKeys("Tech Mahindra");
    driver.findElementById("createLeadForm_firstName").sendKeys("Rajesh");
    driver.findElementById("createLeadForm_lastName").sendKeys("Kumar");
    driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Rajesh");
    driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Kumar");
    driver.findElementById("createLeadForm_personalTitle").sendKeys("Test Lead");

    WebElement src = driver.findElementById("createLeadForm_dataSourceId");
    Select srcdrop = new Select (src);
    srcdrop.selectByVisibleText("Conference");
    
    driver.findElementById("createLeadForm_annualRevenue").sendKeys("8.0 Lakhs");
    
    WebElement src1 = driver.findElementById("createLeadForm_industryEnumId");
    Select industryoption = new Select(src1);
    industryoption.selectByVisibleText("Computer Software");
    
    WebElement src3 = driver.findElementById("createLeadForm_ownershipEnumId");
    Select owneroption = new Select(src3);
    owneroption.selectByVisibleText("S-Corporation");
    
    driver.findElementById("createLeadForm_sicCode").sendKeys("12345");
    driver.findElementById("createLeadForm_description").sendKeys("Testing");
    driver.findElementById("createLeadForm_importantNote").sendKeys("Important");
    
    driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
    
    driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
    
    
    driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("04566");
    
    driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("1456");
    
    driver.findElementById("createLeadForm_departmentName").sendKeys("QC");
    
    WebElement src4 = driver.findElementById("createLeadForm_currencyUomId");
    
    Select currencyoption = new Select(src4);
    
    currencyoption.selectByVisibleText("INR - Indian Rupee");
    
    driver.findElementById("createLeadForm_numberEmployees").sendKeys("10");
    
    driver.findElementById("createLeadForm_tickerSymbol").sendKeys("1245");
    
    driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Narayan");
    
    driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://techmahindra.com");
    
    driver.findElementById("createLeadForm_generalToName").sendKeys("Tech");

    driver.findElementById("createLeadForm_generalAddress1").sendKeys("No.1458/elcot");
    
    driver.findElementById("createLeadForm_generalAddress2").sendKeys("shollinganallur");
    
    driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
    
    WebElement src5 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
    
    Select stateoption = new Select(src5);
    
    
    stateoption.selectByVisibleText("Texas");
    
    WebElement src6 = driver.findElementById("createLeadForm_generalCountryGeoId");
    
    Select countryoption = new Select(src6);
    
    countryoption.selectByVisibleText("India");
    
    Thread.sleep(5000);
    
    WebElement State = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
    
    Select stateoption1 = new Select(State);
    
    stateoption1.selectByVisibleText("TAMILNADU");
    
    driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600063");
    
    driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("15");
    
    WebElement src8 = driver.findElementById("createLeadForm_marketingCampaignId");

    Select marketingoption = new Select(src8);
    
    marketingoption.selectByVisibleText("Automobile");
    
    driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9500543516");
    
    driver.findElementById("createLeadForm_primaryEmail").sendKeys("rajeshliving@gmail.com");

    driver.findElementByXPath("//input[@value='Create Lead']").click();
    
    WebElement firstname = driver.findElementById("viewLead_firstName_sp");
    
    String firstnametext = firstname.getText();
    
    System.out.println("The Firstname is:"+firstnametext);
    
    if (firstnametext.equals(firstnametext))
    {
    	System.out.println("Pass");
    } else 
    	
    {
    	System.out.println("Fail");
    	
    }
    
    driver.close();
    }

}
