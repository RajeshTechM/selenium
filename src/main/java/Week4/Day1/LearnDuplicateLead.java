package Week4.Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnDuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver(); 
		// Implicit wait
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("Demosalesmanager");
	    driver.findElementById("password").sendKeys("crmsfa");
	    driver.findElementByXPath("//input[@type='submit']").click();
	    driver.findElementByLinkText("CRM/SFA").click();
	    driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		Thread.sleep(5000);
		String LeadFirstName = driver.findElementByXPath("(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a)[1]").getText();
		driver.findElementByXPath("(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a)[1]").click();
		System.out.println(LeadFirstName);
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		String TitleDup = driver.findElementById("sectionHeaderTitle_leads").getText();
		System.out.println("The Title of the Page is:"+TitleDup);
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		
		String Capturedtext = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();
		
		
		if(LeadFirstName.equals(Capturedtext)) {
			System.out.println("duplicated lead name is " + Capturedtext + " same as " +Capturedtext);
		} 
		else 
		{
			System.out.println("duplicated lead name is " + Capturedtext + "not same as " +Capturedtext);
		}

	}

}
