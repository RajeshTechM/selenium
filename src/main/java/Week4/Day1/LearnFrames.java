package Week4.Day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrames {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//to maximize window
		driver.manage().window().maximize();
		//to get URL
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		//String name = "koushik";
		Alert promptalert = driver.switchTo().alert();
		//promptalert.sendKeys(name);
		String text = promptalert.getText();
		System.out.println(text);
		
		promptalert.sendKeys("Rajesh");
		promptalert.accept();
	
		String text3 = driver.findElementByXPath("//body/p[@id='demo']").getText();
		System.out.println(text3);
		
		
		
		/*String text2 = driver.findElementById("demo").getText();
		if (text2.contains(name)) {
			System.out.println("pass");
		}else {
			System.out.println("fail");
		}
		
		
		
		
		
		
		WebElement findElementByPartialLinkText = driver.findElementByPartialLinkText("//body/p[@id='demo']");

	//body/p[@id='demo']*/
		
		

	}

}
