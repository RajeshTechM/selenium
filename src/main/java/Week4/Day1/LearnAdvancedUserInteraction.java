package Week4.Day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnAdvancedUserInteraction 
{
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//to maximize the window
		driver.manage().window().maximize();
		//to get the URL
		driver.get("http://jqueryui.com/draggable/");
		driver.switchTo().frame(0);

		Actions actions = new Actions(driver);
		
		WebElement ele = driver.findElementById("draggable");
		//SYNTAX source, X OFFSET, Y OFFSET
		// if we know the target place where it needs to be moved then we have to use drag and drop. //draggable and droppable. 
		//if we have to move from one place and another place without knowing the target place then we have to use drag and drop by. 
		//actions.dragAndDrop(source, target)
		actions.dragAndDropBy(ele, ele.getLocation().getX()+100, ele.getLocation().getY()+100).perform();

	}

}
