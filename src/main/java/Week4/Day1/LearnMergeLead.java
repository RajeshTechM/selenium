package Week4.Day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LearnMergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//to maximize window
			driver.manage().window().maximize();
			 //to get URL
			driver.get("http://leaftaps.com/opentaps/control/main");
			driver.findElementById("username").sendKeys("Demosalesmanager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByXPath("//input[@type='submit']").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Leads").click();
			driver.findElementByLinkText("Merge Leads").click();
			
			driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
			//Window Handles is a method which would return the collection of strings. 
			Set<String> allwindows = driver.getWindowHandles();
			System.out.println(allwindows);
			List<String> alloptions = new ArrayList<String>();
			System.out.println(alloptions);
			//to add all the opened windows
			alloptions.addAll(allwindows);
			
			driver.switchTo().window(alloptions.get(1));
			// to get the title of the window
			String title = driver.getTitle();
			System.out.println(title);
			
			driver.findElementByXPath("//input[@name='firstName']").sendKeys("Rajesh");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(5000);
			
			driver.findElementByXPath("(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]").click();
			driver.switchTo().window(alloptions.get(0));
			String title3 = driver.getTitle();
			System.out.println(title3);
			//driver.switchTo().window(alloptions.get(1));
			driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
			Set<String> allwindows1 = driver.getWindowHandles();
			//System.out.println(allwindows1);
			List<String> alloptions1 = new ArrayList<String>();
			System.out.println(alloptions1);
			//to add all the opened windows
			alloptions1.addAll(allwindows1);
			
			driver.switchTo().window(alloptions1.get(1));
            String title4 = driver.getTitle();
			System.out.println(title4);
			driver.findElementByXPath("//input[@name='firstName']").sendKeys("Rajesh");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(5000);
			driver.findElementByXPath("(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[2]").click();
			driver.switchTo().window(alloptions1.get(0));
			driver.findElementByXPath("//a[text()='Merge']").click();
			
			//handling alert option
			
			driver.switchTo().alert().accept();
			driver.switchTo().window(alloptions1.get(0));
			driver.findElementByLinkText("Find Leads").click();
			Thread.sleep(5000);
			driver.findElementByXPath("(//div[@class='x-form-element']/input)[14]").sendKeys("Rajeshk");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(5000);
			
			String actualerror = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
			System.out.println(actualerror);
			
			 File src = driver.getScreenshotAs(OutputType.FILE);
		       File desc = new File("./snaps/img8.png");
		       FileUtils.copyFile(src, desc);
			
		} 
		
	}

