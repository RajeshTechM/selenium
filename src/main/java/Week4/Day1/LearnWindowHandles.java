package Week4.Day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandles {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe"); 
		ChromeDriver driver = new ChromeDriver(); 
		// to maximize the window 
		driver.manage().window().maximize();
		//to get URL
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allwindows = driver.getWindowHandles();
		System.out.println(allwindows);
		List<String> alloptions = new ArrayList<String>();
		System.out.println(alloptions);
		alloptions.addAll(allwindows);
		
       driver.switchTo().window(alloptions.get(1));
       //driver.manage().window().maximize();
       String title = driver.getTitle();
       String currentUrl = driver.getCurrentUrl();
       System.out.println(title);
       System.out.println(currentUrl);
       
       File src = driver.getScreenshotAs(OutputType.FILE);
       File desc = new File("./snaps/img.png");
       FileUtils.copyFile(src, desc);
       
		driver.switchTo().window(alloptions.get(0));
		driver.close();
		
		

	}

}
