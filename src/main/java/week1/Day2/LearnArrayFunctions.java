package week1.Day2;

public class LearnArrayFunctions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] numbers = {1,5,7,9,11}; 
		
		//Print all the array elements
		
		//sytax for (Datatype variable: array)
	      for (int element: numbers) {
	         System.out.println(element);	
	      }
	}

}
