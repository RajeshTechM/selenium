package week1.day1;

public class ConditionalStatement_StaticValue 
{

	public static void main(String[] args) 
	
	{
		
		int age =45;  // declaring a variable
		
		if (age>=18)  // checking the condition, if its true below statement will be executed. 
		
		{ 
			System.out.println("you are eligible for voting"); // statements
			
		}
			
		else                             // if the condition is false below statement will be executed. 
		{
				
				System.out.println("you are not eligible for voting"); // statements 
				
			}
			
		}
		
		
	}
	
