package Week6.Day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] FetchDataExcel(String excelFileName) throws IOException {
		
		//we have to create a object  //locate the file
     XSSFWorkbook wbook = new XSSFWorkbook("./data/"+excelFileName+".xlsx");
    //-- get sheet of index 0
     XSSFSheet sheet = wbook.getSheetAt(0);
     // to get the row count
     int rowcount = sheet.getLastRowNum();
     //to get the column count
     short columncount = sheet.getRow(0).getLastCellNum();
     Object[][] data = new Object [rowcount][columncount];
 // iterating over all the rows and columns
    for (int j = 1; j <= rowcount; j++) {
		XSSFRow row = sheet.getRow(j);
		for (int i = 0; i < columncount; i++) {
			XSSFCell cell = row.getCell(i);
			data[j-1][i] = cell.getStringCellValue();
			//System.out.println(cellvalue);
		} 
	
  }
    
    return data;
     
		
		
	}
	
	
	
}
