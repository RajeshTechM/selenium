package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



@RunWith(Cucumber.class)

@CucumberOptions (features= "src/test/java/features/CreateLead.feature", glue = {"steps"}, tags = {"@positive"}, monochrome=true,
plugin= {"pretty","html:reports/cucumber"})

public class RunTest {
	
	
}
