Feature: Login to TestLeaf 
Background:
Given open the chrome browser
	And  maximize the window 
	And  set the timeout 
	And  load the URL 
	
@positive
Scenario: Positive flow Login 
	And Enter the username as DemoSalesManager 
	And Enter the password as crmsfa
	When Click on Login button
	Then Verify the login is success
	Then close the browser 
	

Scenario: Negative flow Login 
	And Enter the username as DemoSalesManager 
	And Enter the password as crmsf1
	When Click on Login button
	Then Verify the login is not success
	Then close the browser
@positive
Scenario Outline: Positive flow to Create a Test Lead 
	And Enter the username as <userName>
	And Enter the password as <password>
	When Click on Login button
	Then Verify the login is success 
	And Click on CRMSFA Link 
	And Click on CreateLead Link 
	And Enter the CompanyName as <companyName>
	#And Enter the CompanyName as <companyName>
	And Enter the firstname as <firstName>
	And Enter the Lastname as <lastName>
	When Click on CreateLead
	Then verify the testlead is created successfully 
	Then close the browser
	
Examples: 
|userName|password|companyName|firstName|lastName|
|DemoSalesManager|crmsfa|Tech Mahindra|Rajesh|Kumar|
|DemoSalesManager|crmsfa|Wipro|Bharathi|Kannan|
