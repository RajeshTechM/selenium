package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class StepsForCreatingTestLead extends ProjectMethods {

    public ChromeDriver driver;
    
    @Given("open the chrome browser")
	public void openBrowser() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	
	}

    @And("maximize the window")
	public void maxBrowser() {
		
		driver.manage().window().maximize();
		//reportStep("Driver Maximized successfully", "Pass");
		
}
	@And("set the timeout")
	public void setTimeouts() {
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	@And("load the URL")
	public void loadURL() {
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		
	}
	@And ("Enter the username as (.*)")
	public void enterUserName (String userName) {
		
	driver.findElementById("username").sendKeys(userName);
	
	}
	
	
	@And("Enter the password as (.*)")
	public void enterPassword (String password) {
		
		driver.findElementById("password").sendKeys(password);
		
	}
	
	@When("Click on Login button")
	public void clickLogin () {
		
		driver.findElementByXPath("//input[@type='submit']").click();
		
	}
	
	@Then("Verify the login is success")
	public void verifyLoginSucess () {
		
		System.out.println("Login is successfull");
		
	}
	@Then("Verify the login is not success")
	public void verifyNotSuccess () {
		
	System.out.println("Login is not successful");
	 
	}
	
	@And("Click on CRMSFA Link")
	public void clickCRMSFALink() {
		
		driver.findElementByLinkText("CRM/SFA").click();
		
	}
	
	@And("Click on CreateLead Link")
	public void clickCreateLeadLink() {
		
		driver.findElementByLinkText("Create Lead").click();
	}
	
	
	@And("Enter the CompanyName as (.*)")
	public void EnterTheCompanyName(String companyName) {
		
		driver.findElementById("createLeadForm_companyName").sendKeys(companyName);
	}
	
	
	@And("Enter the firstname as (.*)")
	public void EnterFirstName(String firstName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);
		
	}
	
	
	@And("Enter the Lastname as (.*)")
	public void EnterLastName(String lastName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
		
	}
	
	@When("Click on CreateLead")
	public void clickCreateLead () {
		//click("//input[@value='Create Lead']");
		driver.findElementByXPath("//input[@value='Create Lead']").click();
	}
	
	@Then("verify the testlead is created successfully")
	public void verifyTestLead() 
	{
		 System.out.println("TestLead is created successfully");
	}
	
	
	@Then ("close the browser")
	public void closebrowser() 
		{
			driver.close();
		}
	}





